Rails.application.routes.draw do
  resources :booking_states
  resources :bookings
  resources :users
  resources :books
  resources :contactos
  match "/static_pages/books" => "static_pages#books", :via => :get
  match "/static_pages/help" => "static_pages#help", :via => :get
  match "/static_pages/about" => "static_pages#about", :via => :get
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
